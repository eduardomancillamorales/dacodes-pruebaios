//
//  UIViewcontroller+loadNib.swift
//  MoviesApp
//
//  Created by eduardo mancilla on 09/12/20.
//

import UIKit

extension UIViewController {
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }

        return instantiateFromNib()
    }
}
