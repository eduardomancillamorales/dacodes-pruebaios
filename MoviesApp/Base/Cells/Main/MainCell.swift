//
//  MainCell.swift
//  MoviesApp
//
//  Created by eduardo mancilla on 08/12/20.
//

import UIKit

class MainCell: UICollectionViewCell {
    @IBOutlet weak var imageviewMoview: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var containerView: UIView!
    private var currentSession: URLSessionDataTask?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        containerView.layer.cornerRadius = 10
        containerView.clipsToBounds = true
    }
    
    func configView(data: MainData) {
        labelTitle.text = data.title
        labelDate.text = data.date
        labelRating.text = "\(data.rating)"
        loadImage(imagePath: data.image)
    }
    
    private func loadImage(imagePath: String) {
        let urlBase: String = "https://image.tmdb.org/t/p/w500"
        let urlFull: String = urlBase + imagePath
        self.currentSession = imageviewMoview.downloaded(from: urlFull)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        currentSession?.cancel()
    }
}
