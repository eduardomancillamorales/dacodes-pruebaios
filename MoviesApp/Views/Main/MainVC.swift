//
//  ViewController.swift
//  MoviesApp
//
//  Created by eduardo mancilla on 08/12/20.
//

import UIKit

class MainVC: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    private let spacing:CGFloat = 16.0
    let reuseIdentifier = "cell"
    var items = [MainData]()
    private let service = MainService()
    var refreshControl = UIRefreshControl()
    private var currentPage = 1
    private var currentLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Películas"
        setup()
        getData()
    }
    
    private func setup() {
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        self.collectionView?.collectionViewLayout = layout
        
        let nib = UINib(nibName: "MainCell", bundle: nil)
            collectionView?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        
    }
    
    @objc func refresh(_ sender: AnyObject) {
        currentPage = 1
        self.items = [MainData]()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func getData() {
        guard !currentLoading else { return }
        currentLoading = true
        service.getData(currentPage: currentPage) { result in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.currentLoading = false
                self.collectionView.reloadData()
            }
            switch result {
              case .success(let items):
                self.items.append(contentsOf: items)
                self.currentPage += 1
              case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
}

extension MainVC: UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            getData()
        }
    }
    
}

extension MainVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! MainCell
        cell.configView(data: items[indexPath.row])
        return cell
    }

}

extension MainVC: UICollectionViewDelegate {
 
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailVC.loadFromNib()
        vc.data = items[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension MainVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let w: CGFloat = collectionViewWidth / 2 - 10
        return CGSize(width: w, height: w * 1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 5, bottom: 10, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
