//
//  MainModel.swift
//  MoviesApp
//
//  Created by eduardo mancilla on 09/12/20.
//

import Foundation

struct MainData {
    let id: Int
    let title: String
    let date: String
    let rating: Float
    let image: String
}
