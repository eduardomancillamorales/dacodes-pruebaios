//
//  MainService.swift
//  MoviesApp
//
//  Created by eduardo mancilla on 09/12/20.
//

import Foundation

struct ContainCod: Codable {
    let page: Int
    let results: [Result]
    let totalPages, totalResults: Int

    enum CodingKeys: String, CodingKey {
        case page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
    
    struct Result: Codable {
        let adult: Bool
        let backdropPath: String
        let genreIDS: [Int]
        let id: Int
        let originalLanguage, originalTitle, overview: String
        let popularity: Double
        let posterPath, releaseDate, title: String
        let video: Bool
        let voteAverage: Double
        let voteCount: Int

        enum CodingKeys: String, CodingKey {
            case adult
            case backdropPath = "backdrop_path"
            case genreIDS = "genre_ids"
            case id
            case originalLanguage = "original_language"
            case originalTitle = "original_title"
            case overview, popularity
            case posterPath = "poster_path"
            case releaseDate = "release_date"
            case title, video
            case voteAverage = "vote_average"
            case voteCount = "vote_count"
        }
    }
}

enum NetworkError: Error {
    case badURL
    case serviceError
}

class MainService {
    
    func getData(currentPage: Int, completion: @escaping (Result<[MainData], NetworkError>) -> Void) {
        let endPoint = "https://api.themoviedb.org/3/movie/now_playing?api_key=634b49e294bd1ff87914e7b9d014daed&language=en-US&page=\(currentPage)"
        var listData = [MainData]()
        guard let url = URL(string: endPoint) else { return }
        let request = URLRequest(url: url)
        print("Se va a obtener datos de la pagina: \(currentPage)")
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                if let decodedResponse = try? JSONDecoder().decode(ContainCod.self, from: data) {
                    for result in decodedResponse.results {
                        listData.append(
                            MainData(id: result.id ,title: result.originalTitle, date: result.releaseDate, rating: Float(result.voteAverage), image: result.posterPath)
                        )
                    }
                    completion(.success(listData))
                }
            } else {
                print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
                completion(.failure(.serviceError))
            }
        }.resume()
        
    }
}
