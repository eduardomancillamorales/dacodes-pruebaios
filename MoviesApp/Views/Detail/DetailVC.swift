//
//  DetailVC.swift
//  MoviesApp
//
//  Created by eduardo mancilla on 09/12/20.
//

import UIKit

class DetailVC: UIViewController {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var labelGender: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    private let service = DetailService()
    var data: MainData?
    private var currentSession: URLSessionDataTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateData(item: DetailData(title: "", duration: 0, date: "", rating: 0, gender: [""], description: "", image: ""))
        self.update()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        currentSession?.cancel()
    }

    private func update() {
        guard let data = data else {
            updateData(item: DetailData(title: "", duration: 0, date: "", rating: 0, gender: [""], description: "", image: ""))
            return
        }
        
        service.getData(id: data.id) { result in
            switch result {
              case .success(let item):
                DispatchQueue.main.async {
                    self.updateData(item: item)
                }
              case .failure(let error):
                  print(error.localizedDescription)
            }
        }
    }
    
    private func updateData(item: DetailData) {
        guard self.isViewLoaded else { return }
        labelTitle.text = item.title
        labelDuration.text = "\(item.duration) min"
        labelDate.text = item.date
        labelRating.text = "\(item.rating)"
        labelGender.text = item.gender.joined(separator: ",")
        labelDescription.text = item.description
        loadImage(imagePath: item.image)
    }
    
    private func loadImage(imagePath: String) {
        let urlBase: String = "https://image.tmdb.org/t/p/w500"
        let urlFull: String = urlBase + imagePath
        self.currentSession = imageView.downloaded(from: urlFull, contentMode: .scaleAspectFill)
    }

}
