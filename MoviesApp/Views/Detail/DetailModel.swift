//
//  DetailModel.swift
//  MoviesApp
//
//  Created by eduardo mancilla on 09/12/20.
//

import Foundation

struct DetailData {
    let title: String
    let duration: Int
    let date: String
    let rating: Float
    let gender: [String]
    let description: String
    let image: String
}
