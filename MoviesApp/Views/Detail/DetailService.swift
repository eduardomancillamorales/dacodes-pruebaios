//
//  DetailService.swift
//  MoviesApp
//
//  Created by eduardo mancilla on 09/12/20.
//

import Foundation

struct DetailCod: Codable {
    let adult: Bool
    let backdropPath: String
    let gender: [Gender]
    let id: Int
    let imdbID, originalLanguage, originalTitle, overview: String
    let popularity: Double
    let posterPath: String
    let releaseDate: String
    let revenue, runtime: Int
    let status, tagline, title: String
    let video: Bool
    let voteAverage: Double
    let voteCount: Int

    enum CodingKeys: String, CodingKey {
        case adult
        case backdropPath = "backdrop_path"
        case gender = "genres"
        case id
        case imdbID = "imdb_id"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview, popularity
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case revenue, runtime
        case status, tagline, title, video
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}

struct Gender: Codable {
    let id: Int
    let name: String
}

class DetailService {
    func getData(id: Int, completion: @escaping (Result<DetailData, NetworkError>) -> Void) {
        let endPoint = "https://api.themoviedb.org/3/movie/\(id)?api_key=634b49e294bd1ff87914e7b9d014daed&language=es-mx"
        guard let url = URL(string: endPoint) else { return }
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                if let decodedResponse = try? JSONDecoder().decode(DetailCod.self, from: data) {
                    let data = DetailData(
                        title: decodedResponse.originalTitle,
                        duration: decodedResponse.runtime,
                        date: decodedResponse.releaseDate,
                        rating: Float(decodedResponse.voteAverage),
                        gender: decodedResponse.gender.map { $0.name },
                        description: decodedResponse.overview,
                        image: decodedResponse.backdropPath
                    )
                    completion(.success(data))
                }
            } else {
                print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
                completion(.failure(.serviceError))
            }
        }.resume()
        
    }
}

